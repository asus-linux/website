+++
title = "supergfxctl-plasmoid"
weight = 2
+++

supergfxctl-plasmoid is exactly what it sounds like - a plasmoid to switch between graphics modes via supergfxctl in KDE Plasma. It talks via D-Bus with supergfxd and supports displaying the current power status of the dGPU.

![supergfxctl-plasmoid screenshot](/images/third-party/supergfxctl-plasmoid.jpg)

You can check out the source code at [https://gitlab.com/Jhyub/supergfxctl-plasmoid](https://gitlab.com/Jhyub/supergfxctl-plasmoid)