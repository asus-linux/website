+++
title = "Starting 2024 With A New Look!"
date = 2024-01-02

[extra]
author = "Kyle McGrath"
+++

If you're a long-time visitor of our site, you've likely noticed the drastic changes in our site.

Coming into 2024 I felt it was worth looking into modernising the look of our site. It may take some getting used to, but hopefully you enjoy the fresh new look! I've also taken care to ensure that the site is more readable and easier to navigate on mobile devices.

If you have any comments or feedback, or something doesn't seem to be working right, please let me know on our Discord in the [#asus-linux-website](https://discord.com/channels/725125934759411753/833954788004855859) channel! All testing was done on Firefox with minimal testing on Chrome/Edge.

Additionally, there are still a few loose ends to tie up:
* A light theme for the site
* System light/dark mode detection from browser settings

Hope you enjoy! :smile: