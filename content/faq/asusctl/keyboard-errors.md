+++
title = "Why am I getting errors about my keyboard?"
+++

Please ensure you are using a recent kernel. As of now the minimum is 5.11, but please use at least 5.13 so that you get all the most recent patches and fixes for ASUS laptops.