+++
title = "FAQ"
sort_by = "weight"
template = "sections/faq.html"
+++

The below is a collection of questions that we receive quite frequently - either via GitLab reports or our Discord community. Before opening an issue on GitLab or asking in our Discord, check here first to see if your question has already been addressed!

If nothing below addresses your issue, please ask in this appropriate channel on [our Discord server](https://discord.gg/z8y99XqPb7).