+++
title = "Privacy Policy"
date = 2022-01-01

[extra]
link_name = "Privacy Policy"
+++

## We use Matomo
Matomo is an open source web analytics platform. A web analytics platform is used by a website owner in order to measure, collect, analyse and report visitor data for purposes of understanding and optimizing their website.

## Purpose of the processing

Matomo is used to get an idea how our website is used by our users so we can optimize things further so that the user may find the most relevant information more quickly.

### Data Matomo is processing
* User IP address
* Optional User ID
* Date and time of the request
* Title of the page being viewed
* URL of the page being viewed
* Referrer URL
* Screen resolution being used
* Time in local user’s timezone
* Files that were clicked and downloaded
* Links to an outside domain that were clicked
* Page generation time
* User location: country, region, city, approximate latitude and longitude
* Main Language of the browser being used
* User Agent of the browser being used


#### Pick up the one you are using:
* Cookies
* IP address
* User ID
* Custom Dimensions
* Custom Variables
* Location of the user

And also:

* Date and time
* Title of the page being viewed
* URL of the page being viewed
* URL of the page that was viewed prior to the current page
* Screen resolution
* Time in local timezone
* Files that were clicked and downloaded
* Link clicks to an outside domain
* Pages generation time
* Country, region, city
* Main Language of the browser
* User Agent of the browser

This list can be completed with additional features such as:

* Form interactions
* Media interactions
* A/B Tests

The processing of personal data with Matomo is based on legitimate interests


### The right to withdraw consent at any time

If you wish us to not process any personal data with Matomo, you can opt-out from it at any time. There will be no consequences at all regarding the use of our website.

You can withdraw at any time your consent by clicking here
<iframe style="border: 0; height: 200px; width: 600px; max-width: 100%;"
src="https://stats.asus-linux.org/index.php?module=CoreAdminHome&action=optOut&language=en"
></iframe>


### The right to lodge a complaint with a supervisory authority

If you think that the way we process your personal data with Matomo analytics is infringing the law, you have the right to lodge a complaint with a supervisory authority.

### We are not doing any profiling.